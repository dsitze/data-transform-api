package com.dsitze.sample.csv;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.MarshallerException;
import com.dsitze.sample.metrics.performance.Site;

public class MarshallingTest {
    private static final String testCsv = "site_id, site_name, site_location, host_id, host_name, ip_address, operative_system, load_avg_1min, load_avg_5min, load_avg_15min\n"
                                                + "1, site-1, location-1, 1001, Host1, 168.1.1.1, \"Windows\",  1.0,  1.0, 1.0\n"
                                                + "1, site-1, location-1, 1002, Host2, 168.1.1.2, \"Solaris\", 14.3, 12.5, 5.5\n"
                                                + "2, site-2, location-2, 2001, Host1, 168.2.1.1,   \"Linux\",  \"3.2\",  1.4, 1.1\n";
    CSVData                     root;
    Site                        site1;
    Site                        site2;
    Host                        host1_1;
    Host                        host1_2;
    Host                        host2_1;
    CSVFormatMarshaller marshaller;
    List<Host>                  hosts;

    @Before
    public void setup() throws Exception {
        marshaller = new CSVFormatMarshaller();
        site1 = createSite((short) 1, "site-1", "location-1");
        site2 = createSite((short) 2, "site-2", "location-2");
        hosts = new ArrayList<Host>(3);
        host1_1 = createHost(site1, (short) 1001, "Host1", "168.1.1.1", "Windows", 1.0f, 1.0f, 1.0f);
        host1_2 = createHost(site1, (short) 1002, "Host2", "168.1.1.2", "Solaris", 14.3f, 12.5f, 5.5f);
        host2_1 = createHost(site2, (short) 2001, "Host1", "168.2.1.1", "Linux", 3.2f, 1.4f, 1.1f);
        root = new CSVTransformer().toFormat(hosts);
    }

    private Host createHost(Site site, short id, String name,
            String ip, String os, float load1, float load5, float load15) {
        Host host = new Host();
        host.setSite(site);
        host.setId(id);
        host.setName(name);
        host.setIpAddress(ip);
        host.setOsType(os);
        host.setLoadAverage_1Minute(load1);
        host.setLoadAverage_5Minute(load5);
        host.setLoadAverage_15Minute(load15);
        hosts.add(host);
        return host;
    }

    private Site createSite(short id, String name, String location) {
        Site createSite = new Site();
        createSite.setId(id);
        createSite.setName(name);
        createSite.setLocation(location);
        return createSite;
    }

    @Test
    public void testUnmarshalling() throws MarshallerException, IOException {
        InputStream is = new ByteArrayInputStream(testCsv.getBytes());
        CSVData result = marshaller.read(is);
        is.close();
        /*
        System.out.println("---------testUnmarshalling----------");
        System.out.println(toCSVString(result));
        System.out.println("------------------------------------");
        */
        assertEquals(3, result.hosts.size());
        assertEquals(host1_1, result.hosts.get(0).toHost());
        assertEquals(host1_2, result.hosts.get(1).toHost());
        assertEquals(host2_1, result.hosts.get(2).toHost());
    }

    @Test
    public void testMarshalling() throws MarshallerException, IOException {
        String result = toCSVString(root);
        /*
        System.out.println("----------testMarshalling-----------");
        System.out.println(result);
        System.out.println("------------------------------------");
        */
        String expected = testCsv.replaceAll("(?s)[ \"]+", "");
        assertEquals(expected,result);
    }

    private String toCSVString(CSVData root) throws MarshallerException,
            IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        marshaller.write(root, os);
        os.flush();
        os.close();
        String result = new String(os.toByteArray());
        return result;
    }

}
