package com.dsitze.sample.csv;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.Site;

public class TransformTest {
	List<Host> sourceList;

	@Before
	public void setup() {
		final Site site1 = createSite((short) 1, "Site1", "Location1");
		final Site site2 = createSite((short) 2, "Site2", "Location2");
		sourceList = new ArrayList<Host>();
		sourceList.add(new Host() {
			{
				setSite(site1);
				setIpAddress("127.0.0.3");
				setName("FirstHost");
				setLoadAverage_1Minute(1.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setSite(site2);
				setIpAddress("127.0.0.1");
				setName("SecondHost");
				setLoadAverage_1Minute(2.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setSite(site1);
				setIpAddress("127.0.0.2");
				setName("FirstSystem");
				setLoadAverage_1Minute(1.0f);
			}
		});
	}

	private Site createSite(short id, String name, String location) {
		Site createSite = new Site();
		createSite.setId(id);
		createSite.setName(name);
		createSite.setLocation(location);
		return createSite;
	}

	@Test
	public void testTransform() {
		CSVTransformer transformer = new CSVTransformer();
		List<Host> result = transformer.fromFormat(transformer
				.toFormat(sourceList));
		assertEquals(sourceList, result);
	}

}
