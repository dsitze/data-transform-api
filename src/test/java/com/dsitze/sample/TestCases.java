package com.dsitze.sample;

import com.dsitze.sample.csv.CSVData;
import com.dsitze.sample.csv.CSVFormatter;
import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.PerformanceCountersTransformer;
import com.dsitze.sample.metrics.performance.sort.Order;
import com.dsitze.sample.metrics.performance.sort.Sort;
import com.dsitze.sample.xml.Sites;
import com.dsitze.sample.xml.XMLFormatter;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static com.dsitze.sample.metrics.performance.sort.Direction.ASCENDING;
import static com.dsitze.sample.metrics.performance.sort.Direction.DESCENDING;
import static com.dsitze.sample.metrics.performance.sort.Order.by;
import static com.dsitze.sample.metrics.performance.sort.SortField.*;

/**
 * Functional API tests:
 * <dl>
 * <dt>Test Case #1</dt>
 * <dd>Generate XML output from CSV input. Output is sorted by operating system,
 * then by descending host_id. No filter.</dd>
 * <dt>Test Case #2</dt>
 * <dd>Generate XML output from XML input. Output is sorted by load average,
 * then by descending host_id. No filter.</dd>
 * <dt>Test Case #3</dt>
 * <dd>Generate XML output from CSV input. Output is sorted by host_name. Filter
 * by site_name = 'NY-01'</dd>
 * <dt>Test Case #4</dt>
 * <dd>Generate CSV output from XML input. Output is sorted by location, operating
 * system, then descending load average.</dd>
 * </dl>
 *
 * @author Donald Sitze
 */
public class TestCases {
    private Formatter<List<Host>, CSVData> csvFormatter;
    private Formatter<List<Host>, Sites> xmlFormatter;
    PerformanceCountersTransformer<?, ?> transformer;

    @Before
    public void setUp() throws Exception {
        csvFormatter = new CSVFormatter();
        xmlFormatter = new XMLFormatter();
    }

    @Test
    public void testCase1() throws MarshallerException, IOException {
        setTransformer(
                csvFormatter,
                xmlFormatter,
                null,
                by(HOST_OS, ASCENDING), by(HOST_ID, DESCENDING)
        );
        transformer.transform(
                "target/test-classes/perf_counters_csv_sample_data.csv",
                "target/test_case_1_output.xml"
        );
    }

    @Test
    public void testCase2() throws MarshallerException, IOException {
        setTransformer(
                xmlFormatter,
                xmlFormatter,
                null,
                by(HOST_LOAD_AVG_1_MIN, ASCENDING), by(HOST_ID, DESCENDING)
        );
        transformer.transform(
                "target/test-classes/perf_counters_xml_sample_data.xml",
                "target/test_case_2_output.xml"
        );
    }

    @Test
    public void testCase3() throws MarshallerException, IOException {
        setTransformer(
                csvFormatter,
                xmlFormatter,
                host -> Objects.equals("NY-01", host.getSite().getName()),
                by(HOST_NAME, ASCENDING));
        transformer.transform(
                "target/test-classes/perf_counters_csv_sample_data.csv",
                "target/test_case_3_output.xml"
        );
    }

    @Test
    public void testCase4() throws MarshallerException, IOException {
        setTransformer(
                xmlFormatter,
                csvFormatter,
                null,
                by(SITE_LOCATION, ASCENDING), by(HOST_OS, ASCENDING), by(HOST_LOAD_AVG_1_MIN, DESCENDING)
        );
        transformer.transform(
                "target/test-classes/perf_counters_xml_sample_data.xml",
                "target/test_case_4_output.csv"
        );
    }

    private <I, O> void setTransformer(
            Formatter<List<Host>, I> from,
            Formatter<List<Host>, O> to,
            Predicate<Host> filter,
            Order... orders) {
        transformer = new PerformanceCountersTransformer<>(from, to, filter, new Sort(orders));
    }
}
