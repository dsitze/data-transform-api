package com.dsitze.sample.xml;

import com.dsitze.sample.MarshallerException;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class MarshallingTest {
    private static final String testXml = "<Sites xmlns=\"http://sample.dsitze.com/performance_metrics.xsd\">"
            + " <Site id=\"1\" name=\"site-1\" location=\"location-1\">"
            + "  <Hosts>"
            + "   <Host id=\"1001\">"
            + "    <Host_Name>Host1</Host_Name>"
            + "    <IP_address>168.1.1.1</IP_address>"
            + "    <OS>Windows</OS><Load_avg_1min>1.0</Load_avg_1min>"
            + "    <Load_avg_5min>1.0</Load_avg_5min>"
            + "    <Load_avg_15min>1.0</Load_avg_15min>"
            + "   </Host>"
            + "   <Host id=\"1002\">"
            + "    <Host_Name>Host2</Host_Name>"
            + "    <IP_address>168.1.1.2</IP_address>"
            + "    <OS>Solaris</OS>"
            + "    <Load_avg_1min>14.3</Load_avg_1min>"
            + "    <Load_avg_5min>12.5</Load_avg_5min>"
            + "    <Load_avg_15min>5.5</Load_avg_15min>"
            + "   </Host>"
            + "  </Hosts>"
            + " </Site>"
            + " <Site id=\"2\" name=\"site-2\" location=\"location-2\">"
            + "  <Hosts>"
            + "   <Host id=\"2001\">"
            + "    <Host_Name>Host1</Host_Name>"
            + "    <IP_address>168.2.1.1</IP_address>"
            + "    <OS>Linux</OS>"
            + "    <Load_avg_1min>3.2</Load_avg_1min>"
            + "    <Load_avg_5min>1.4</Load_avg_5min>"
            + "    <Load_avg_15min>1.1</Load_avg_15min>"
            + "   </Host>"
            + "  </Hosts>"
            + " </Site>"
            + "</Sites>";

    private Sites root;
    private Host host1_1;
    private Host host1_2;
    private Host host2_1;
    private XMLFormatMarshaller marshaller;

    @Before
    public void setup() throws Exception {
        marshaller = new XMLFormatMarshaller();
        marshaller.setFormattedOutput(false);
        root = new Sites();
        root.site = new ArrayList<>();
        Site site1 = createSite((short) 1, "site-1", "location-1");
        Site site2 = createSite((short) 2, "site-2", "location-2");
        host1_1 = createHost(site1, (short) 1001, "Host1", "168.1.1.1", "Windows", 1.0f, 1.0f, 1.0f);
        host1_2 = createHost(site1, (short) 1002, "Host2", "168.1.1.2", "Solaris", 14.3f, 12.5f, 5.5f);
        host2_1 = createHost(site2, (short) 2001, "Host1", "168.2.1.1", "Linux", 3.2f, 1.4f, 1.1f);
    }

    private Host createHost(Site site, short id, String name,
                            String ip, String os, float load1, float load5, float load15) {
        Host host = new Host();
        host.id = id;
        host.hostName = name;
        host.ipAddress = ip;
        host.os = os;
        host.loadAvg1Min = load1;
        host.loadAvg5Min = load5;
        host.loadAvg15Min = load15;
        site.hosts.add(host);
        return host;
    }

    private Site createSite(short id, String name, String location) {
        Site createSite = new Site();
        createSite.id = id;
        createSite.name = name;
        createSite.location = location;
        createSite.hosts = new ArrayList<>();
        root.site.add(createSite);
        return createSite;
    }


    @Test
    public void testUnmarshalling() throws MarshallerException, IOException {
        InputStream is = new ByteArrayInputStream(testXml.getBytes());
        Sites result = marshaller.read(is);
        is.close();

        assertEquals("Number of sites: ", 2, result.site.size());
        assertEquals("Number of hosts in 1st site: ", 2, result.site.get(0).hosts.size());
        assertEquals("Number of hosts in 2nd site: ", 1, result.site.get(1).hosts.size());
        assertEquals(host1_1, result.site.get(0).hosts.get(0));
        assertEquals(host1_2, result.site.get(0).hosts.get(1));
        assertEquals(host2_1, result.site.get(1).hosts.get(0));
    }

    @Test
    public void testMarshalling() throws MarshallerException, IOException {
        String result = toXMLString(root);
        String regex = "<[?]xml version=\"1[.]0\"[^>]*><Sites[^>]*>\\s*"
                + "<(\\S*:)?Site[^>]*>\\s*"
                + "<(\\S*:)?Hosts>\\s*"
                + "(<(\\S*:)?Host id=\"[^\"]+\">\\s*"
                + "<Host_Name>[^<>]*</Host_Name>"
                + "<IP_address>[^<>]*</IP_address>"
                + "<OS>[^<>]*</OS>"
                + "<Load_avg_1min>[0-9.]+</Load_avg_1min>"
                + "<Load_avg_5min>[0-9.]+</Load_avg_5min>"
                + "<Load_avg_15min>[0-9.]+</Load_avg_15min>"
                + "</(\\S*:)?Host>){2}"
                + "</(\\S*:)?Hosts>"
                + "</(\\S*:)?Site>"
                + "<(\\S*:)?Site.*>"
                + "<(\\S*:)?Hosts>[:space:]*"
                + "(<(\\S*:)?Host id=\"[^\"]+\">"
                + "<Host_Name>[^<>]*</Host_Name>"
                + "<IP_address>[^<>]*</IP_address>"
                + "<OS>[^<>]*</OS>"
                + "<Load_avg_1min>[0-9.]+</Load_avg_1min>"
                + "<Load_avg_5min>[0-9.]+</Load_avg_5min>"
                + "<Load_avg_15min>[0-9.]+</Load_avg_15min>"
                + "</(\\S*:)?Host>){1}"
                + "</(\\S*:)?Hosts>\\s*"
                + "</(\\S*:)?Site>\\s*"
                + "</Sites>";
        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(result);
        assertTrue("output mismatch", matcher.matches());

    }

    private String toXMLString(Sites root) throws MarshallerException,
            IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        marshaller.write(root, os);
        os.flush();
        os.close();
        return new String(os.toByteArray());
    }
}
