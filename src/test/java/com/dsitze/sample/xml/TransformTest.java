package com.dsitze.sample.xml;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.Site;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransformTest {
	List<Host> sourceList;

	@Before
	public void setup() {
		final Site site1 = createSite((short) 1, "Site1", "Location1");
		final Site site2 = createSite((short) 2, "Site2", "Location2");
		sourceList = new ArrayList<>();
		sourceList.add(new Host() {
			{
				setSite(site1);
				setIpAddress("127.0.0.3");
				setName("FirstHost");
				setLoadAverage_1Minute(3.0f);
				setLoadAverage_5Minute(2.0f);
				setLoadAverage_15Minute(1.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setSite(site2);
				setIpAddress("127.0.0.1");
				setName("SecondHost");
				setLoadAverage_1Minute(6.0f);
				setLoadAverage_5Minute(4.0f);
				setLoadAverage_15Minute(2.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setSite(site1);
				setIpAddress("127.0.0.2");
				setName("FirstSystem");
				setLoadAverage_1Minute(3.0f);
				setLoadAverage_5Minute(2.0f);
				setLoadAverage_15Minute(1.0f);
			}
		});
	}

	private Site createSite(short id, String name, String location) {
		Site createSite = new Site();
		createSite.setId(id);
		createSite.setName(name);
		createSite.setLocation(location);
		return createSite;
	}

	@Test
	public void testTransform() {
		XMLTransformer transformer = new XMLTransformer();
		List<Host> result = transformer.fromFormat(transformer
				.toFormat(sourceList));
		assertEquals(sourceList, result);
	}
}
