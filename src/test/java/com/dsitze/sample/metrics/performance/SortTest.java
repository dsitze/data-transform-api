package com.dsitze.sample.metrics.performance;

import com.dsitze.sample.metrics.performance.sort.Direction;
import com.dsitze.sample.metrics.performance.sort.Order;
import com.dsitze.sample.metrics.performance.sort.Sort;
import com.dsitze.sample.metrics.performance.sort.SortField;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SortTest {
	private List<Host> sourceList;

	@Before
	public void setup() {
		sourceList = new ArrayList<>();
		sourceList.add(new Host() {
			{
				setIpAddress("127.0.0.3");
				setName("FirstHost");
				setLoadAverage_1Minute(1.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setIpAddress("127.0.0.1");
				setName("SecondHost");
				setLoadAverage_1Minute(2.0f);
			}
		});
		sourceList.add(new Host() {
			{
				setIpAddress("127.0.0.2");
				setName("FirstSystem");
				setLoadAverage_1Minute(1.0f);
			}
		});
	}

	private List<Host> newOrder(Integer... order) {
		assertEquals(order.length, sourceList.size());
		List<Host> copy = new ArrayList<>(sourceList.size());
		for (Integer entry : order) {
			copy.add(sourceList.get(entry));
		}
		return copy;
	}

	@Test
	public void testNoSort() {
		Sort sort = new Sort();
		List<Host> expected = newOrder(0, 1, 2);
		sourceList.sort(sort);
		assertEquals(expected, sourceList);
	}

	@Test
	public void testOneKeySort() {
		Sort sort = new Sort(
				new Order(SortField.HOST_NAME, Direction.ASCENDING));
		List<Host> expected = newOrder(0, 2, 1);
		sourceList.sort(sort);
		assertEquals(expected, sourceList);
	}

	@Test
	public void testMultiKeySort() {
		Sort sort = new Sort(
				new Order(SortField.HOST_LOAD_AVG_1_MIN, Direction.DESCENDING),
				new Order(SortField.HOST_IP, Direction.ASCENDING));
		List<Host> expected = newOrder(1, 2, 0);
		sourceList.sort(sort);
		assertEquals(expected, sourceList);
	}

}
