package com.dsitze.sample.metrics.performance.sort;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.Site;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class HostComparatorFactory {
    @SuppressWarnings("serial")
    static private final Map<SortField, Comparator<Host>> comparators = new HashMap<SortField, Comparator<Host>>() {

        {
            put(SortField.SITE_ID, new SiteComparator() {

                @Override
                public int compareSite(Site s1, Site s2) {
                    Short id1 = s1.getId();
                    Short id2 = s2.getId();
                    return compareInstance(id1, id2);
                }
            });
            put(SortField.SITE_NAME, new SiteComparator() {

                @Override
                public int compareSite(Site s1, Site s2) {
                    String n1 = s1.getName();
                    String n2 = s2.getName();
                    return compareInstance(n1, n2);
                }
            });
            put(SortField.SITE_LOCATION, new SiteComparator() {

                @Override
                public int compareSite(Site s1, Site s2) {
                    Short id1 = s1.getId();
                    Short id2 = s2.getId();
                    return compareInstance(id1, id2);
                }
            });
            put(SortField.HOST_ID, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getId(), h2.getId());
                }
            });
            put(SortField.HOST_NAME, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getName(), h2.getName());
                }
            });
            put(SortField.HOST_IP, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getIpAddress(), h2.getIpAddress());
                }
            });
            put(SortField.HOST_OS, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getOsType(), h2.getOsType());
                }
            });
            put(SortField.HOST_LOAD_AVG_1_MIN, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getLoadAverage_1Minute(),
                            h2.getLoadAverage_1Minute());
                }
            });
            put(SortField.HOST_LOAD_AVG_5_MIN, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getLoadAverage_5Minute(),
                            h2.getLoadAverage_5Minute());
                }
            });
            put(SortField.HOST_LOAD_AVG_15_MIN, new HostComparator() {

                @Override
                protected int compareHost(Host h1, Host h2) {
                    return compareInstance(h1.getLoadAverage_15Minute(),
                            h2.getLoadAverage_15Minute());
                }
            });
        }
    };

    static Comparator<Host> sortBy(SortField field, Direction direction) {
        Comparator<Host> comparator = comparators.get(field);
        if (null == comparator) {
            throw new IllegalStateException("No comparator defined for " + field.name());
        }
        return direction.wrap(comparator);
    }

    static Comparator<Host> sortBy(Order order) {
        return sortBy(order.field, order.direction);
    }

}
