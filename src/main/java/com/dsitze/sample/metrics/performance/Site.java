package com.dsitze.sample.metrics.performance;

/**
 * Representation of information indicating the location of a hosting center for
 * a {@link Host}.
 *
 * @author Donald Sitze
 */
public class Site {
    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    private Short id;

    private String name;

    private String location;

    @Override
    public boolean equals(Object o) {
        boolean isEqual = false;
        if (null != o) {
            if (o instanceof Site) {
                Site s = (Site) o;
                isEqual = equals(id, s.getId())
                        && equals(name, s.getName())
                        && equals(location, s.getLocation());
            }
        }
        return isEqual;
    }

    private boolean equals(Object o1, Object o2) {
        boolean isEquals = false;
        if (null != o1) {
            isEquals = o1.equals(o2);
        } else {
            isEquals = (null == o2);
        }
        return isEquals;
    }

    @Override
    public int hashCode() {
        int hashCode = 0;
        if (null != id) {
            hashCode += id.hashCode();
        }
        if (null != name) {
            hashCode += name.hashCode();
        }
        if (null != location) {
            hashCode += location.hashCode();
        }
        return hashCode;
    }

    @Override
    public String toString() {
        return String.format("Site[%d,%s,%s]", id, name, location);
    }
}
