package com.dsitze.sample.metrics.performance.sort;

public enum SortField {
    SITE_ID,
    SITE_NAME,
    SITE_LOCATION,
    HOST_ID,
    HOST_NAME,
    HOST_IP,
    HOST_OS,
    HOST_LOAD_AVG_1_MIN,
    HOST_LOAD_AVG_5_MIN,
    HOST_LOAD_AVG_15_MIN;
}