package com.dsitze.sample.metrics.performance.sort;

import com.dsitze.sample.metrics.performance.Host;

import java.util.Comparator;

abstract class HostComparator implements Comparator<Host> {

    @Override
    public int compare(Host h1, Host h2) {
        int compare = compareReference(h1, h2);
        if (2 == compare) {
            compare = compareHost(h1, h2);
        }
        return compare;
    }

    protected abstract int compareHost(Host h1, Host h2);

    protected <T> int compareInstance(Comparable<T> c1, T c2) {
        int compare = compareReference(c1, c2);
        if (2 == compare) {
            compare = c1.compareTo(c2);
        }
        return compare;
    }

    protected int compareReference(Object o1, Object o2) {
        int compare = 0;
        if (o1 != o2) {
            if (null == o1) {
                compare = 1;
            } else if (null == o2) {
                compare = -1;
            } else {
                compare = 2;
            }
        }
        return compare;
    }
}