package com.dsitze.sample.metrics.performance.sort;

import com.dsitze.sample.metrics.performance.Host;

import java.util.Comparator;
import java.util.function.Function;

public enum Direction {
    ASCENDING(c -> c),
    DESCENDING(Comparator::reversed);

    private final Function<Comparator<Host>, Comparator<Host>> wrapper;

    Direction(Function<Comparator<Host>, Comparator<Host>> wrapper) {
        this.wrapper = wrapper;
    }

    protected Comparator<Host> wrap(Comparator<Host> c) {
        return wrapper.apply(c);
    }
}