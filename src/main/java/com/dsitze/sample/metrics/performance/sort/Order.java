package com.dsitze.sample.metrics.performance.sort;

public class Order {
    public SortField field;
    public Direction direction;

    public Order(SortField field, Direction direction) {
        this.field = field;
        this.direction = direction;
    }

    public static Order by(SortField field, Direction direction) {
        return new Order(field, direction);
    }
}
