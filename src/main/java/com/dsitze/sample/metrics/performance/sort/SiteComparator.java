package com.dsitze.sample.metrics.performance.sort;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.Site;

abstract class SiteComparator extends HostComparator {

    @Override
    public int compareHost(Host h1, Host h2) {
        Site s1 = h1.getSite();
        Site s2 = h2.getSite();
        int compare = compareReference(s1, s2);
        if (2 == compare) {
            compare = compareSite(s1, s2);
        }
        return compare;
    }

    abstract public int compareSite(Site s1, Site s2);

}