package com.dsitze.sample.metrics.performance;

/**
 * Representation of identifying attributes and performance metrics for a Host.
 * A Host is identified by the combination of Site, name, IP Address, OS type
 * and a numeric ID. The performance metrics provided are load averages over the
 * last 1, 5, and 15 minutes.
 *
 * @author Donald Sitze
 */
public class Host {
    private Site site;

    private Short id;

    private String name;

    private String ipAddress;

    private String osType;

    private Float loadAverage_1Minute;

    private Float loadAverage_5Minute;

    private Float loadAverage_15Minute;

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    public Float getLoadAverage_1Minute() {
        return loadAverage_1Minute;
    }

    public void setLoadAverage_1Minute(Float loadAverage_1Minute) {
        this.loadAverage_1Minute = loadAverage_1Minute;
    }

    public Float getLoadAverage_5Minute() {
        return loadAverage_5Minute;
    }

    public void setLoadAverage_5Minute(Float loadAverage_5Minute) {
        this.loadAverage_5Minute = loadAverage_5Minute;
    }

    public Float getLoadAverage_15Minute() {
        return loadAverage_15Minute;
    }

    public void setLoadAverage_15Minute(Float loadAverage_15Minute) {
        this.loadAverage_15Minute = loadAverage_15Minute;
    }

    private boolean equals(Object o1, Object o2) {
        boolean isEquals = false;
        if (null != o1) {
            isEquals = o1.equals(o2);
        } else {
            isEquals = (null == o2);
        }
        return isEquals;
    }

    @Override
    public boolean equals(Object o) {
        boolean isEquals = false;
        if (null != o && o instanceof Host) {
            Host h = (Host) o;
            isEquals = equals(site, h.getSite())
                    && equals(id, h.getId())
                    && equals(name, h.getName())
                    && equals(ipAddress, h.getIpAddress())
                    && equals(osType, h.getOsType())
                    && equals(loadAverage_1Minute, h.getLoadAverage_1Minute())
                    && equals(loadAverage_5Minute, h.getLoadAverage_5Minute())
                    && equals(loadAverage_15Minute, h.getLoadAverage_15Minute());
        }
        return isEquals;
    }

    @Override
    public String toString() {
        return String.format("Host[%s,%d,%s,%s,%s,%.1f,%.1f,%.1f]", site.toString(), id, name, ipAddress, osType, loadAverage_1Minute, loadAverage_5Minute, loadAverage_15Minute);
    }
}
