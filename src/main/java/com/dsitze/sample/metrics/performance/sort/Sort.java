package com.dsitze.sample.metrics.performance.sort;

import com.dsitze.sample.metrics.performance.Host;

import java.util.Comparator;
import java.util.stream.Stream;

import static java.util.stream.Stream.empty;

/**
 * Provides sorting for lists of Host.
 *
 * @author Donald Sitze
 */
public class Sort implements Comparator<Host> {
    private final Comparator<Host> orderBy;

    /**
     * Creates a Sort instance that applies the provided Order selection.
     * Precedence is applied from first (highest) to last (lowest).
     * If none specified, no ordering is imposed.
     */
    public Sort(Order... orders) {
        this.orderBy = orderBy((null == orders) ? empty() : Stream.of(orders).map(HostComparatorFactory::sortBy));
    }

    private Comparator<Host> orderBy(final Stream<Comparator<Host>> by) {
         return by.reduce(Comparator::thenComparing).orElse((a, b) -> 0);
    }

    public int compare(Host a, Host b) {
        return this.orderBy.compare(a, b);
    }

}
