package com.dsitze.sample.metrics.performance;

import com.dsitze.sample.Formatter;
import com.dsitze.sample.BaseTransformer;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

/**
 * Provide API to transform performance counter data for a list of {@link Host} objects across
 * formats and with optional filtering and re-ordering of records.
 *
 * @author Donald Sitze
 */
public class PerformanceCountersTransformer<INPUT, OUTPUT> extends BaseTransformer<Host, INPUT, OUTPUT> {
    /**
     * Construct a PerformanceCountersTransformer for a desired transformation
     * scenario.
     *
     * @param from      Formatter of input file.
     * @param to        Formatter of output file.
     * @param filter    filter criteria to select included records in output.
     * @param sortOrder ordering criteria for output record order.
     */
    public PerformanceCountersTransformer(Formatter<List<Host>, INPUT> from, Formatter<List<Host>, OUTPUT> to,
                                          Predicate<Host> filter, Comparator<Host> sortOrder) {
        super(from, to, filter, sortOrder);
    }
}
