package com.dsitze.sample.xml;

import com.dsitze.sample.FormatTransformer;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public class XMLTransformer implements FormatTransformer<List<com.dsitze.sample.metrics.performance.Host>,Sites> {

    @Override
    public Sites toFormat(List<com.dsitze.sample.metrics.performance.Host> hosts) {
        Sites sites = new Sites();
        sites.site = new ArrayList<>();
        Site site = null;
        for (com.dsitze.sample.metrics.performance.Host host : hosts) {
            Host xmlHost = toXmlHost(host);
            if (!isSameSite(site, host.getSite())) {
                maybeAdd(sites, site);
                site = toXmlSite(host.getSite());
            }
            site.hosts.add(xmlHost);
        }
        maybeAdd(sites, site);
        return sites;
    }

    private void maybeAdd(Sites sites, Site site) {
        if (null != site) {
            sites.site.add(site);
        }
    }

    private boolean isSameSite(Site site, com.dsitze.sample.metrics.performance.Site site1) {
        return nonNull(site)
                && Objects.equals(site.id, site1.getId())
                && Objects.equals(site.name, site1.getName())
                && Objects.equals(site.location, site1.getLocation());
    }

    private Host toXmlHost(com.dsitze.sample.metrics.performance.Host host) {
        Host toHost = new Host();
        toHost.setId(host.getId());
        toHost.setHostName(host.getName());
        toHost.setIPAddress(host.getIpAddress());
        toHost.setOS(host.getOsType());
        toHost.setLoadAvg1Min(forNull(host.getLoadAverage_1Minute()));
        toHost.setLoadAvg5Min(forNull(host.getLoadAverage_5Minute()));
        toHost.setLoadAvg15Min(forNull(host.getLoadAverage_15Minute()));
        return toHost;
    }

    private Float forNull(Float v) {
        return forNull(v, 0.0f);
    }

    private <T> T forNull(T optional, T ifMissing) {
        return Optional.ofNullable(optional).orElse(ifMissing);
    }

    private Site toXmlSite(com.dsitze.sample.metrics.performance.Site site) {
        Site toSite = new Site();
        toSite.setId(site.getId());
        toSite.setName(site.getName());
        toSite.setLocation(site.getLocation());
        toSite.hosts = new ArrayList<>();
        return toSite;
    }

    @Override
    public List<com.dsitze.sample.metrics.performance.Host> fromFormat(Sites format) {
        return Optional.ofNullable(format)
                .map(Sites::getSite)
                .orElseGet(Collections::emptyList)
                .stream()
                .flatMap(this::siteHosts)
                .collect(toList());
    }

    private Stream<com.dsitze.sample.metrics.performance.Host> siteHosts(Site site) {
        com.dsitze.sample.metrics.performance.Site toSite = toSite(site);
        return site.getHosts()
                .stream()
                .map(toHost(toSite));
    }

    private com.dsitze.sample.metrics.performance.Site toSite(Site site) {
        com.dsitze.sample.metrics.performance.Site toSite = new com.dsitze.sample.metrics.performance.Site();
        toSite.setId(site.getId());
        toSite.setName(site.getName());
        toSite.setLocation(site.getLocation());
        return toSite;
    }

    private Function<Host, com.dsitze.sample.metrics.performance.Host> toHost(com.dsitze.sample.metrics.performance.Site site) {
        return host -> {
            com.dsitze.sample.metrics.performance.Host toHost = new com.dsitze.sample.metrics.performance.Host();
            toHost.setId(host.getId());
            toHost.setName(host.getHostName());
            toHost.setIpAddress(host.getIPAddress());
            toHost.setOsType(host.getOS());
            toHost.setLoadAverage_1Minute(host.getLoadAvg1Min());
            toHost.setLoadAverage_5Minute(host.getLoadAvg5Min());
            toHost.setLoadAverage_15Minute(host.getLoadAvg15Min());
            toHost.setSite(site);
            return toHost;
        };
    }

}
