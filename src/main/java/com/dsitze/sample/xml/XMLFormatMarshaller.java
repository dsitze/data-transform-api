package com.dsitze.sample.xml;

import com.dsitze.sample.FormatMarshaller;
import com.dsitze.sample.MarshallerException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;

import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;

public class XMLFormatMarshaller implements FormatMarshaller<Sites> {
    private JAXBContext context;
    private Boolean formattedOutput = true;
    private ObjectFactory objectFactory;

    XMLFormatMarshaller() throws MarshallerException {
        try {
            context = JAXBContext.newInstance(ObjectFactory.class);
            objectFactory = new ObjectFactory();
        } catch (JAXBException e) {
            throw new MarshallerException(e);
        }
    }

    @Override
    public Sites read(InputStream stream) throws MarshallerException {
        try {
            return Optional.ofNullable(context.createUnmarshaller().unmarshal(stream))
                    .filter(JAXBElement.class::isInstance)
                    .map(JAXBElement.class::cast)
                    .map(JAXBElement::getValue)
                    .filter(Sites.class::isInstance)
                    .map(Sites.class::cast)
                    .orElseThrow(() -> new UnmarshalException("No valid data found."));
        } catch (JAXBException e) {
            throw new MarshallerException(e);
        }
    }

    @Override
    public void write(Sites format, OutputStream stream)
            throws MarshallerException {
        try {
            javax.xml.bind.Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(JAXB_FORMATTED_OUTPUT, formattedOutput);
            marshaller.marshal(objectFactory.createSites(format), stream);
        } catch (JAXBException e) {
            throw new MarshallerException(e);
        }
    }

    void setFormattedOutput(Boolean formattedOutput) {
        this.formattedOutput = formattedOutput;
    }

}
