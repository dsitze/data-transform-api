package com.dsitze.sample.xml;

import com.dsitze.sample.Formatter;
import com.dsitze.sample.MarshallerException;
import com.dsitze.sample.metrics.performance.Host;

import java.util.List;

public class XMLFormatter extends Formatter<List<Host>, Sites> {

    public XMLFormatter() throws MarshallerException {
        super(new XMLTransformer(), new XMLFormatMarshaller());
    }

}
