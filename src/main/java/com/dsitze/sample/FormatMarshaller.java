package com.dsitze.sample;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * I/O interface for generic data type T.
 *
 * @param <T> type to be marshalled.
 */
public interface FormatMarshaller<T> {
    /**
     * Read an instance from provided input stream.
     *
     * @param stream InputStream from which data is read.
     * @return An instance of <code>T</code>
     * @throws MarshallerException if an error occurs while processing the input.
     */
    T read(InputStream stream) throws MarshallerException;

    /**
     * Write an instance to the provided output stream.
     *
     * @param instance output data.
     * @param stream   OutputStream on which data is written.
     * @throws MarshallerException if an error occurs while processing the output.
     */
    void write(T instance, OutputStream stream) throws MarshallerException;
}
