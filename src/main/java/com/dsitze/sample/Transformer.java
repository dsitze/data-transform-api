package com.dsitze.sample;

import java.io.File;
import java.io.IOException;

/**
 * Provides API to transform across two different record formats using an internal intermediary object model
 * representation, with optional filtering and re-ordering of records.
 *
 * @param <R>      internal object model representation (robots in disguise).
 * @param <INPUT>  input format type
 * @param <OUTPUT> target output format type
 * @author Donald Sitze
 */
public interface Transformer<R, INPUT, OUTPUT> {
    /**
     * Apply the transformation represented by this
     * PerformanceCountersTransformer on the data from source to produce the
     * output file destination.
     *
     * @param source      file to read INPUT data from
     * @param destination file to write OUTPUT data to.
     * @throws MarshallerException if problems with format conversion occur
     * @throws IOException       if problems with file reading/writing occur.
     */
    void transform(File source, File destination) throws MarshallerException, IOException;

    /**
     * Apply the transformation represented by this
     * PerformanceCountersTransformer on the data from then file named by source to produce the
     * output file named by destination.
     *
     * @param sourceFileName      name of file to read INPUT data from.
     * @param destinationFileName name of file to write OUTPUT data to.
     * @throws MarshallerException if problems with format conversion occur.
     * @throws IOException       if problems with file reading/writing occur.
     */
    void transform(String sourceFileName, String destinationFileName) throws MarshallerException, IOException;
}
