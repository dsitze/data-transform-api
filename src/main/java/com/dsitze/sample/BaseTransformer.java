package com.dsitze.sample;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Generic implementation of {@link Transformer} API.
 * @param <R> Representative type of an individual record within the input and output formats.
 * @param <INPUT> Input format type.
 * @param <OUTPUT> Output format type
 */
public class BaseTransformer<R, INPUT, OUTPUT> implements Transformer<R, INPUT, OUTPUT> {
    private final Formatter<List<R>, OUTPUT> toFormatter;
    private final Formatter<List<R>, INPUT> fromFormatter;
    private final Predicate<R> filter;
    private final Comparator<R> sortOrder;

    /**
     * Construct a Transformer for a desired transformations.
     *
     * @param from      Formatter of input file.
     * @param to        Formatter of output file.
     * @param filter    filter criteria to select included records in output.
     * @param sortOrder ordering criteria for output record order.
     */
    public BaseTransformer(Formatter<List<R>, INPUT> from, Formatter<List<R>, OUTPUT> to, Predicate<R> filter, Comparator<R> sortOrder) {
        this.fromFormatter = from;
        this.toFormatter = to;
        this.filter = (null == filter) ? x -> true : filter;
        this.sortOrder = (null == sortOrder) ? (a, b) -> 0 : sortOrder;
    }

    @Override
    public void transform(File source, File destination)
            throws MarshallerException, IOException {
        List<R> data = fromFormatter.load(source).stream().filter(filter).sorted(sortOrder).collect(Collectors.toList());
        toFormatter.save(data, destination);
    }

    @Override
    public void transform(String sourceFileName, String destinationFileName)
            throws MarshallerException, IOException {
        File source = new File(sourceFileName);
        File destination = new File(destinationFileName);
        this.transform(source, destination);
    }
}
