package com.dsitze.sample;

import com.dsitze.sample.metrics.performance.Host;

import java.io.*;

/**
 * Binds type-specific transformation and data marshaling objects, and provides
 * I/O methods that operate through those objects.
 * <p>
 * Support of a new file format can be enabled by creating an appropriate format
 * specific sub-class of this class, and implementing the needed object model
 * and its associated transformer and formatMarshaller classes.
 *
 * @param <R> Type for the representative object model.
 * @param <F> Type for the format specific object model.
 * @author Donald Sitze
 */
public abstract class Formatter<R, F> {
    private final FormatTransformer<R, F> transformer;
    private final FormatMarshaller<F> formatMarshaller;

    protected Formatter(FormatTransformer<R, F> transformer, FormatMarshaller<F> formatMarshaller) {
        this.transformer = transformer;
        this.formatMarshaller = formatMarshaller;
    }

    /**
     * Load data using <code>F</code> as an intermediary format from specified file. The data is
     * read into the data model of type <code>F</code>, and will be then transformed into
     * the common data representation as an instance of <code>R</code>.
     *
     * @param file {@link File} from which data is to be read.
     * @return an instance of <code>R</code> as the common representation of the data from file.
     * @throws MarshallerException if there are parsing errors on the input file.
     * @throws IOException       if there are file I/O errors while reading/accessing the file.
     */
    public R load(File file) throws MarshallerException, IOException {
        R data = null;
        try (InputStream is = new FileInputStream(file)) {
            F content = formatMarshaller.read(is);
            data = transformer.fromFormat(content);
        }
        return data;
    }

    /**
     * Save data using <code>F</code> as an intermediary format to specified file. The
     * common data model of a list of {@link Host} will be transformed into the
     * object model of type <code>F</code>, which will then be marshaled to the output
     * file.
     *
     * @param data list of {@link Host Hosts} to be saved.
     * @param file File in which formatted output is to be written.
     * @throws MarshallerException if there are parser errors while producing the target output
     *                           format.
     * @throws IOException       if there are file I/O errors while writing/accessing the
     *                           output file.
     */
    public void save(R data, File file) throws MarshallerException, IOException {
        try (OutputStream os = new FileOutputStream(file)) {
            F content = transformer.toFormat(data);
            formatMarshaller.write(content, os);
        }
    }

}
