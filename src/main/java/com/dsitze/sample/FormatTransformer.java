package com.dsitze.sample;

/**
 * Provide Transformations between a format specific type and a common data representation.
 *
 * @param <R> representative type
 * @param <F> format specific type.
 * @author Donald Sitze
 */
public interface FormatTransformer<R, F> {
    F toFormat(R representation);

    R fromFormat(F format);
}
