package com.dsitze.sample.csv;

import com.dsitze.sample.FormatMarshaller;
import com.dsitze.sample.MarshallerException;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class CSVFormatMarshaller implements FormatMarshaller<CSVData> {

    @Override
    public CSVData read(InputStream stream) throws MarshallerException {
        CSVData read = new CSVData();
        List<CSVHost> csvHosts = new ArrayList<CSVHost>();
        try {
            Reader reader = new InputStreamReader(stream);
            try (CSVReader csvReader = new CSVReader(reader, ',', '"', 1)) {
                String[] input;
                while (null != (input = csvReader.readNext())) {
                    CSVHost host = new CSVHost(input);
                    csvHosts.add(host);
                }
            }
        } catch (Throwable t) {
            throw new MarshallerException(t);
        }
        read.hosts = csvHosts;
        return read;
    }

    @Override
    public void write(CSVData format, OutputStream stream) throws MarshallerException {
        try {
            Writer writer = new OutputStreamWriter(stream);
            try (CSVWriter csvWriter = new CSVWriter(writer, ',', (char) 0)) {
                csvWriter.writeNext(CSVHost.getCSVHeader());
                for (CSVHost host : format.hosts) {
                    csvWriter.writeNext(host.toStrings());
                }
            }
        } catch (Throwable t) {
            throw new MarshallerException(t);
        }
    }

}
