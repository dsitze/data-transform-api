package com.dsitze.sample.csv;

import com.dsitze.sample.metrics.performance.Host;
import com.dsitze.sample.metrics.performance.Site;

public class CSVHost {
    private static enum FieldIndex {
        SITE_ID,
        SITE_NAME,
        SITE_LOCATION,
        HOST_ID,
        HOST_NAME,
        IP_ADDRESS,
        OPERATIVE_SYSTEM,
        LOAD_AVG_1MIN,
        LOAD_AVG_5MIN,
        LOAD_AVG_15MIN
    }

    public static String[] getCSVHeader() {
        String[] header = new String[FieldIndex.values().length];
        for (FieldIndex i : FieldIndex.values()) {
            header[i.ordinal()] = i.name().toLowerCase();
        }
        return header;
    }

    private abstract class Field<T> {
        private final FieldIndex index;
        private T value;

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public Field(FieldIndex index) {
            this.index = index;
        }

        public void into(String[] output) throws CSVConversionException {
            try {
                insert(output, from(value));
            } catch (Throwable t) {
                throw new CSVConversionException(t);
            }
        }

        public void outOf(String[] input) throws CSVConversionException {
            try {
                value = valueOf(extract(input));
            } catch (Throwable t) {
                throw new CSVConversionException(t);
            }
        }

        protected String extract(String[] input) {
            return input[index.ordinal()];
        }

        protected void insert(String[] output, String value) {
            output[index.ordinal()] = value;
        }

        abstract protected T valueOf(String value);

        protected String from(T value) {
            return value.toString();
        }

        protected String cleanUp(String input) {
            return input.trim().replace("\"", "");
        }
    }

    private class StringField extends Field<String> {

        public StringField(FieldIndex index) {
            super(index);
        }

        @Override
        protected String valueOf(String value) {
            return cleanUp(value);
        }


    }

    private class ShortField extends Field<Short> {

        public ShortField(FieldIndex index) {
            super(index);
        }

        @Override
        protected Short valueOf(String value) {
            return Short.valueOf(cleanUp(value));
        }

    }

    private class FloatField extends Field<Float> {

        public FloatField(FieldIndex index) {
            super(index);
        }

        @Override
        protected Float valueOf(String value) {
            return Float.valueOf(cleanUp(value));
        }

    }

    private final ShortField site_id = new ShortField(FieldIndex.SITE_ID);
    private final StringField site_name = new StringField(FieldIndex.SITE_NAME);
    private final StringField site_location = new StringField(FieldIndex.SITE_LOCATION);
    private final ShortField host_id = new ShortField(FieldIndex.HOST_ID);
    private final StringField host_name = new StringField(FieldIndex.HOST_NAME);
    private final StringField ip_address = new StringField(FieldIndex.IP_ADDRESS);
    private final StringField operative_system = new StringField(FieldIndex.OPERATIVE_SYSTEM);
    private final FloatField load_avg_1min = new FloatField(FieldIndex.LOAD_AVG_1MIN);
    private final FloatField load_avg_5min = new FloatField(FieldIndex.LOAD_AVG_5MIN);
    private final FloatField load_avg_15min = new FloatField(FieldIndex.LOAD_AVG_15MIN);

    private final Field<?> fields[] = {
            site_id,
            site_name,
            site_location,
            host_id,
            host_name,
            ip_address,
            operative_system,
            load_avg_1min,
            load_avg_5min,
            load_avg_15min};

    public CSVHost() {
    }

    CSVHost(Host host) {
        if (null != host) {
            Site site = host.getSite();
            if (null != site) {
                site_id.setValue(site.getId());
                site_name.setValue(site.getName());
                site_location.setValue(site.getLocation());
            }
            host_id.setValue(host.getId());
            host_name.setValue(host.getName());
            ip_address.setValue(host.getIpAddress());
            operative_system.setValue(host.getOsType());
            load_avg_1min.setValue(host.getLoadAverage_1Minute());
            load_avg_5min.setValue(host.getLoadAverage_5Minute());
            load_avg_15min.setValue(host.getLoadAverage_15Minute());
        }
    }

    CSVHost(String[] values) throws CSVConversionException {
        for (Field<?> f : fields) {
            f.outOf(values);
        }
    }

    Host toHost() {
        Site site = new Site();
        site.setId(site_id.getValue());
        site.setName(site_name.getValue());
        site.setLocation(site_location.getValue());
        Host host = new Host();
        host.setSite(site);
        host.setId(host_id.getValue());
        host.setName(host_name.getValue());
        host.setOsType(operative_system.getValue());
        host.setIpAddress(ip_address.getValue());
        host.setLoadAverage_1Minute(load_avg_1min.getValue());
        host.setLoadAverage_5Minute(load_avg_5min.getValue());
        host.setLoadAverage_15Minute(load_avg_15min.getValue());
        return host;
    }

    String[] toStrings() throws CSVConversionException {
        String[] strings = new String[fields.length];
        for (Field<?> f : fields) {
            f.into(strings);
        }
        return strings;
    }

}