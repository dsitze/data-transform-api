package com.dsitze.sample.csv;

import com.dsitze.sample.FormatTransformer;
import com.dsitze.sample.metrics.performance.Host;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public class CSVTransformer implements FormatTransformer<List<Host>, CSVData> {

    @Override
    public CSVData toFormat(List<Host> hosts) {
        CSVData csvData = new CSVData();
        csvData.hosts = convertHosts(hosts);
        return csvData;
    }

    @Override
    public List<Host> fromFormat(CSVData data) {
        return convertCSVHosts((null != data) ? data.hosts : null);
    }

    private List<Host> convertCSVHosts(List<CSVHost> source) {
        return convert(source, CSVHost::toHost);
    }

    private List<CSVHost> convertHosts(List<Host> source) {
        return convert(source, CSVHost::new);
    }

    private <T, F> List<T> convert(List<F> source, Function<F, T> transform) {
        return Optional.ofNullable(source).orElseGet(Collections::emptyList).stream()
                .map(transform)
                .collect(toList());
    }
}
