package com.dsitze.sample.csv;

import com.dsitze.sample.Formatter;
import com.dsitze.sample.metrics.performance.Host;

import java.util.List;

public class CSVFormatter extends Formatter<List<Host>, CSVData> {

    public CSVFormatter() {
        super(new CSVTransformer(), new CSVFormatMarshaller());
    }

}
