package com.dsitze.sample.csv;

/**
 * Thrown when an error occurs while converting data to/from CSV object model.
 *
 * @author Donald Sitze
 */
@SuppressWarnings("serial")
public class CSVConversionException extends Exception {

    /**
     * Construct a CSVConversionException.
     *
     * @param cause reason for conversion failure.
     */
    public CSVConversionException(Throwable cause) {
        super(cause);
    }

}
