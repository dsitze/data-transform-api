package com.dsitze.sample;

@SuppressWarnings("serial")
/**
 * Exception thrown when an error occurs during marshalling operations.
 */
public class MarshallerException extends Exception {
    public MarshallerException(Throwable cause) {
        super(cause);
    }
}
