# Data Transformation Coding Sample #

* An API that accepts a set of data in one format, and outputs the data in a possibly different format.
* The API provides the ability to filter and to sort the records by one or more fields (e.g. filter by site_name, or sort by site_location ASCENDING and by host_name DESCENDING).
* The existing implementation handles CSV and XML formats, but you should be able to plug in new formats with minimal effort.
* The API allows transformation in both directions for implemented formats.
* The existing formats supplied are XML and CSV data representing performance metrics for a set of hosts at one or more sites.

## Design Decisions/Assumptions ##

### Assumptions ###

* Won't be dealing with extremely large input sizes, so the entire data-set can be handled in-memory.

### Design Decisions ###

* Maven will be used as the build tool and to execute tests.
* XML handling will be done with Java standard JAXB library
* CSV handling will be done with the [opencsv](http://opencsv.sourceforge.net/) library, chosen
  for relative simplicity of API.
* format independent representation of the data will be used
  internally for filtering and sorting operations.
* I/O will be performed through format-specific handlers, using
  transformations from core format to format specific object models.
* New formats may be added by implementing appropriate format
  formatMarshaller, transformer and format definition classes, along
  with an (optional) appropriate format-specific object model. A
  set of interfaces for the three required components is provided.

## Compile/Test Instructions ##

Project uses Maven to manage build. As a result standard maven commands can be
used to compile and execute the tests. The simplest command that builds and
executes the suite of tests is:

```
	mvn test
```

There are four test-cases demonstrating the functionality of the transformation library. These
are implemented in the `TestCases` class under the test source tree. The scenarios they demonstrate
are as follows:

1. Generate XML output from CSV input. Sorting the output by operating system, then by descending host id.
1. Generate XML from XML input, sorted by load average (1 minute average), then descending host_id.
1. Generate XML from CSV, sorted by host name, filtered to select host from sites named 'NY-01'
1. Generate CSV from XML, sorted by location, operating system, descending (1 minute) load average.

Output from the test-cases will be found under the 'target' folder:

* target/test_case_1_output.xml
* target/test_case_2_output.xml
* target/test_case_3_output.xml
* target/test_case_4_output.csv

## Third-Party Libraries ##

The third-party libraries used (i.e. that have direct runtime references) in this
project are as follows:

* [opencsv 3.10](https://opencsv.sourceforge.net/)
   - Provides CSV/Java object binding and read/write for CSV
* [jaxb-basics-runtime 0.11.1](https://github.com/highsource/jaxb2-basics)
   - Provides dependencies needed in generated JAXB classes.
* [junit 4.12](http://junit.org/)
   - JUnit test framework
   
## Build details ##
The project is built using Maven 3.x, and Java 8. In addition to the standard JDK compilation,
the maven build uses these additional plugins for code generation:

* jaxb2-maven-plugin (org.codehaus.mojo:jaxb2-maven-plugin:2.3.1)
  - Provides basic JAXB code generation via standard `xjc`.
* jaxb-xew-plugin (com.github.jaxb-xew-plugin:jaxb-xew-plugin:1.9)
  - xjc plugin used to generate collections instead of wrapper container classes.
* jaxb2-basics (org.jvnet.jaxb2_commons:jaxb2-basics:0.11.1)
  - additional xjc plugin to generate `equals` and `hashCode` on generated JAXB classes.
